const User = require("../models/User.js");
const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

module.exports.checkIfEmailExists = (requestBody) => {

    return User.find({email: requestBody.email}).then(result => {

        if(result.length > 0) {

            return true;

        } else {

            return false;
        }
    })
};

// User Registration
/*
	BUsiness Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (requestBody) => {

    let newUser = new User({
        firstName: requestBody.firstName,
        lastName: requestBody.lastName,
        email: requestBody.email,
        mobileNo: requestBody.mobileNo,
        password: bcrypt.hashSync(requestBody.password, 10)
    });

    return newUser.save().then((user, err) => {

        if(err) {

            return false;

        } else {

            return true;

        }
    })
};


module.exports.authenticateUser = (requestBody) => {

    return User.findOne({email: requestBody.email}).then(result => {

        if(result == null) {
            
            return false;

        } else {

            const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

            if(isPasswordCorrect) {

                return {access: auth.createAccessToken(result)}

            } else {

                return false;
            }
        }
    })
};

module.exports.getProfile = (userData) => {
	return User.findById(userData.userId)
	.then(result => {
		console.log(userData)
		if(result == null) {
			return false
		} else {
			console.log(result)
			result.password = ""
			return result;
		}
	})
};

// Enrolling user to a course
/*
	Business Logic:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
module.exports.enrollUser = async (data) => {

    let isUserUpdated = await User.findById(data.userId).then(user => {

        user.enrollments.push({courseId: data.courseId});

        return user.save().then((enrolled, err) => {

            if (err) {

                return false;

            } else {

                return true;
            }
        })
    });

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {

        course.enrollees.push({userId: data.userId});

        return course.save().then((enroll, err) => {

            if (err) {

                return false;

            } else {

                return true;
            }
        })
    });

    if(isUserUpdated && isCourseUpdated) {

        return true;

    } else {

        return false;

    }
}





